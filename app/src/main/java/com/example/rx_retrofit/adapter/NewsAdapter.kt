import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.retrofitdemo.News
import com.example.rx_retrofit.R

class NewsAdapter(private val context: Context, private val news: List<News>): RecyclerView.Adapter<NewsAdapter.ViewHolder>() {

    class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView){
        val image: ImageView = itemView.findViewById(R.id.imgThumbnail)
        val title: TextView = itemView.findViewById(R.id.text_title)
        val description: TextView = itemView.findViewById(R.id.text_description)
        val date: TextView = itemView.findViewById(R.id.date)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.item,parent,false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = news[position]
        holder.title.text = item.title
        holder.description.text = item.description
        holder.date.text = item.pubDate.substring(0,10)

        if(item.imageUrl == null){
            Glide.with(context).load("https://upload.wikimedia.org/wikipedia/commons/d/d1/Image_not_available.png").into(holder.image)
        }else{
            Glide.with(context).load(item.imageUrl).into(holder.image)
        }
    }


    override fun getItemCount(): Int {
        return news.size
    }

}
