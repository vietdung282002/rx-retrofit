package com.example.rx_retrofit.network

import io.reactivex.Observable
import com.example.retrofitdemo.ListNews

import retrofit2.http.GET
import retrofit2.http.Query

interface APIService {
    @GET("/api/1/news")
    fun getNews(
        @Query("apikey") apiKey: String,
        @Query("language") language: String,
    ): Observable<ListNews>

}

