package com.example.rx_retrofit.network

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object API {
    private var retrofit: Retrofit? = null

    val apiService: APIService
        get() {
            if(retrofit == null){
                retrofit = Retrofit.Builder()
                    .baseUrl("https://newsdata.io")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
            }
            return retrofit!!.create(APIService::class.java)
        }
}