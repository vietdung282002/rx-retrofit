package com.example.retrofitdemo


data class ListNews(
    val results: List<News>
)

data class News(
    val title: String,
    val description: String,
    val pubDate: String,
    val imageUrl: String,
)
