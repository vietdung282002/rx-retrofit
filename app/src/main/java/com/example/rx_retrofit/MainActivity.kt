package com.example.rx_retrofit

import NewsAdapter
import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager.VERTICAL
import com.example.retrofitdemo.ListNews
import com.example.retrofitdemo.News
import com.example.rx_retrofit.network.API
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class MainActivity : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var newsAdapter: NewsAdapter
    private lateinit var btn: Button
    private lateinit var listNews: ListNews
    private var mNews : MutableList<News> = mutableListOf()
    private lateinit var mDisposable: Disposable
    private lateinit var mDialog: Dialog

    private val apiKey = "pub_346686fde36e1541e312917e79cafb55b9658"
    private val language = "en"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mDialog = Dialog(this@MainActivity)
        mDialog.setContentView(R.layout.loading)
        mDialog.window!!.setLayout(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT)

        btn = findViewById(R.id.button)
        initRecyclerView()
        btn.setOnClickListener {
            onClickCallAPI()
        }
    }

    private fun onClickCallAPI() {

        API.apiService.getNews(apiKey,language)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(getListNewObserverRx())
    }

    private fun getListNewObserverRx():Observer<ListNews> {
        mDialog.show()
        return object :Observer<ListNews>{
            override fun onComplete() {
                mDialog.dismiss()
                Toast.makeText(this@MainActivity,"Success",Toast.LENGTH_LONG).show()
                getNewsData()
                newsAdapter = NewsAdapter(applicationContext, mNews)
                recyclerView.adapter = newsAdapter
                recyclerView.adapter!!.notifyDataSetChanged()
            }

            override fun onError(e: Throwable) {
                Toast.makeText(this@MainActivity,"Error",Toast.LENGTH_LONG).show()
            }

            override fun onNext(t: ListNews) {
                listNews = t
            }

            override fun onSubscribe(d: Disposable) {
                mDisposable = d
            }
        }
    }


    private fun initRecyclerView() {
        recyclerView = findViewById(R.id.rvNews)
        recyclerView.layoutManager = LinearLayoutManager(this@MainActivity)

    }

    private fun getNewsData() {
        listNews.results.forEach { news ->
            mNews.add(news)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if(mDisposable != null){
            mDisposable.dispose()
        }
    }
}